<?php
 if(!class_exists('NymlsDb')){
	
	class NymlsDb extends AspkDb{
		
		protected $pkey;
		protected $searchFeilds = array() ;
		protected $detailFeilds =array();
		protected $allowedFields = array();
		Private $error = array();
		protected $setError;
		private $rows_affected;
		private $aspk_nymls_db;
		
		function NymlsDb(){
			$this->pkey = 'nys_mls_id';
			register_activation_hook( 'RT/aspk_Rt_controller.php', array(&$this, 'install') );
			$this->setAllowedFields();
			$this->setResultFields();
			$this->setDetailResultFields();
			$this->allowedFields;
			
			$db_user_id = '';
			$db_password = '';
			$db_name = '';
			$db_host_name = '';
			if(!isset($this->aspk_nymls_db)){
				$this->aspk_nymls_db = new wpdb($db_user_id, $db_password, $db_name, $db_host_name);
			}
			
		}
	
		function install(){
			global $wpdb;
				$sql = "CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."aspkNyPropertyDb` (
				`id` INT( 50 ) NOT NULL ,
				`nys_mls_id` INT( 50 ) NOT NULL ,
				`propertytype` VARCHAR( 255 ) NOT NULL ,
				`bedrooms` VARCHAR( 100 ) NOT NULL ,
				`full_baths` VARCHAR( 100 ) NOT NULL ,
				`half_baths` VARCHAR( 100 ) NOT NULL ,
				`county` TEXT NOT NULL ,
				`area` TEXT NOT NULL ,
				`borough` TEXT NOT NULL ,
				`address` TEXT NOT NULL ,
				`description` LONGTEXT NULL ,
				`currentprice` VARCHAR( 50 ) NOT NULL ,
				`approximateLotSize` VARCHAR( 100 ) NOT NULL ,
				`neighbourhood` VARCHAR( 255 ) NULL ,
				`school` VARCHAR( 255 ) NULL ,
				`yearbuilt` DATE NULL ,
				`status` VARCHAR( 100 ) NOT NULL ,
				`property_img` VARCHAR( 255 ) NOT NULL ,
				`profile_img` VARCHAR( 255 ) NOT NULL ,
				`profile_info` VARCHAR( 255 ) NOT NULL ,
				`acres` VARCHAR( 255 ) NOT NULL ,
				`broker_web_id` VARCHAR( 255 ) NOT NULL ,
				`s_r_rent` VARCHAR( 255 ) NOT NULL ,
				`borough` TEXT NOT NULL ,
				`tax_deduction` TEXT NOT NULL ,
				`per_month_maintenance` TEXT NOT NULL ,
				`stories` TEXT NOT NULL ,
				`city_view` TEXT NOT NULL ,
				`garage` TEXT NOT NULL ,
				`terrace` TEXT NOT NULL ,
				`galley_kitchen` TEXT NOT NULL ,
				`living_room` TEXT NOT NULL ,
				`central_ac` TEXT NOT NULL ,
				`storage` TEXT NOT NULL ,
				`laundry` TEXT NOT NULL ,
				`elevator` TEXT NOT NULL ,
				`pets_allowed` TEXT NOT NULL ,
				`rooftop_access` TEXT NOT NULL ,
				`gym` TEXT NOT NULL ,
				PRIMARY KEY (  `id` )
				) ENGINE = INNODB COMMENT =  'aspkNyPropertyDb' AUTO_INCREMENT=1
				";
				$wpdb->query($sql);	
				
		}
		
		function setAllowedFields(){
			$this->allowedFields = array("nys_mls_id","propertytype","currentprice","county","area","yearbuilt",	"acres","broker_web_id","s_r_rent","borough","neighbourhood","school","property_img","profile_img","profile_info","tax_deduction","per_month_maintenance","stories","bedrooms","full_baths","half_baths","city_view","garage","terrace","galley_kitchen","living_room","central_ac","description","storage","laundry","elevator", "pets_allowed","rooftop_access","gym","status","address");
		}
		function setResultFields($fields = false){
			
			if(is_array($fields)){
				
				$this->searchFeilds = array();
				
				foreach($fields as $f){
					foreach($f as $k=> $v){
						if($this->validateField($k)){
							$this->searchFeilds[$k] = $v;
						}
					}
				}
			}else{
				$this->searchFeilds = array('propertytype','bedrooms','baths','area','address','currentprice','neighbourhood','status','property_img');
			}
		}
		
		function setDetailResultFields($fields = false){
			if(is_array($fields)){
				
				$this->detailFeilds = array();
				
				foreach($fields as $f){
					if($this->validateField($f)){
						$this->detailFeilds[] = $f;
					}
				}
				
			}else{
			$this->detailFeilds = array('nys_mls_id','propertytype','bedrooms','baths',
				'area','address','description','currentprice',
				,'neighbourhood','school','yearbuilt','status','property_img');
			
			}
		}
		function getResultFields(){
			return $this->searchFeilds;
		}
		
		function getDetailResultFields(){
			return $this->detailFeilds;
		}
		function getPrimaryKey(){
			 $pkey = $this->pkey;
			return $pkey;
		}
		function getProperty($propertyId){
			global $wpdb; 
			
			$sql="SELECT * FROM  `{$wpdb->prefix}aspkNyPropertyDb` WHERE {$this->pkey} ={$propertyId}";
			return $wpdb->get_results($sql);
		}
		
		function getPropertyKv($field,$value){
			global $wpdb; 
			
			$sql="SELECT * FROM  `{$wpdb->prefix}aspkNyPropertyDb` WHERE {$field} = '{$value}'";
			return $wpdb->get_row($sql);
		}
		
		function insertProperty($alldata){
			global $wpdb;
			
			$fields="";
			$values="";
			$fieldx="";
			$valuex="";
			foreach($alldata as $k=>$v){
				if(!$v) continue;
				$fieldx .= $k.',';
				$valuex .= "'{$v}',";	
			}
			$fields = substr($fieldx, 0, -1);
			$values = substr($valuex, 0, -1);
			$sql="INSERT INTO {$wpdb->prefix}aspkNyPropertyDb ({$fields}) VALUES({$values})";
			$ret = $wpdb->query($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			
			$rows=$this->rowsAffected();
			return true;
		
		}
		function updateProperty($propertyId,$alldata){
			global $wpdb; 
			
			$arrKv = array();
			foreach($alldata as $key=>$val){
				if(!$val) continue;
				$arrKv[] = "{$key} = '{$val}'";
			}
			$str = implode(' , ', $arrKv);	
			$sql="UPDATE {$wpdb->prefix}aspkNyPropertyDb set {$str} WHERE {$this->pkey}='{$propertyId}'";
			$ret = $wpdb->query($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			return true;
		}
		
		function deleteProperty($propertyId){
			global $wpdb; 
			
			$sql="DELETE FROM {$wpdb->prefix}aspkNyPropertyDb WHERE {$this->pkey} ={$propertyId}";
			$ret = $wpdb->query($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			return true;
		}
		
		
		function getTabelName(){
			global $wpdb; 
			
			$tabelName = $wpdb->prefix.'aspkNyPropertyDb';
			return $tabelName;
		}
	
	}

 }
?>