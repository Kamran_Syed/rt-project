<?php
if ( !class_exists( 'Aspk_Results_View' )){

	class Aspk_Results_View{
		
		function __construct(){
			
		}
		
		private function show_property($property){
			ob_start();
			?>
			<div class="tw-bs container">
				<div class="row aspk_row">
					<div class="col-md-5 aspk_col">
						<img src="<?php echo $property->propertyImg; ?>" class="aspk_p_img">
					</div>
					<div class="col-md-7 aspk_col">
						<div class="row aspk_row">
							<div class="col-md-12"><a href="<?php echo get_permalink().'?matrixUniqueId='. $property->matrixUniqueId.'&mlsId='.$property->mlsId; ?>"><?php  echo $property->address; ?></a></div>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12"><p><?php  echo $property->description; ?></p></div>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12"><?php  echo $property->price; ?></div>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12"><?php  echo $property->area; ?></div>
						</div>
					</div>
				</div>
			</div >
			<?php 
			$html = ob_get_clean();
			return $html;
		}
		
		function show_results($all_results){
			foreach($all_results as $property){
				echo $this->show_property($property);
			}
		}
		
		function show_detail_page($detail){
			?>
			<div class="tw-bs container">
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><img src="<?php echo $detail->propertyImg; ?>" class="aspk_p_img"></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span"><?php echo $detail->address; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span"><?php echo $detail->area; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Status:</span><span class="aspk_span"><?php echo $detail->status; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Price:</span><span class="aspk_span"><?php echo $detail->price; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Property Type:</span><span class="aspk_span"><?php echo $detail->propertyType; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Neighbourhood:</span><span class="aspk_span"><?php echo $detail->neighbourhood; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><h1>Description:</h1></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><p><?php echo $detail->description; ?></p></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><h1>Directions:</h1></div>
				</div>
				 <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
								<script>
								  var geocoder;
								  var map;
								  var mapOptions = {
									  zoom: 14,
									  zoomControl: true,
									  mapTypeId: google.maps.MapTypeId.ROADMAP
									}
								  var marker;

								  jQuery(document).ready(function ($) {
										geocoder = new google.maps.Geocoder();
										map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
										codeAddress();
								  });

								  function codeAddress() {
									var address = "<?php echo $detail->address ?>";
									geocoder.geocode( { 'address': address}, function(results, status) {
									  if (status == google.maps.GeocoderStatus.OK) {
										map.setCenter(results[0].geometry.location);
										if(marker)
										  marker.setMap(null);
										marker = new google.maps.Marker({
											map: map,
											position: results[0].geometry.location,
											draggable: true
										});
									  } else {
										alert('Geocode was not successful for the following reason: ' + status);
									  }
									});
								  }
								</script>
					<div class="row aspk_row">
						<div class="col-md-12 aspk_col" id="map_canvas"></div>
					</div>
					<div class="row aspk_row">
						<div class="col-md-12 aspk_col"><?php echo $detail->address;?></div>
					</div>
			</div>
			<?php
		}
		
	} //class ends
}//if class ends