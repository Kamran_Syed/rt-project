<?php
if ( !class_exists('AspkUser')){
	class AspkUser extends AspkPerson{
		private $phoneNumber;
		private $adress;
		private $request;
		function AspkUser(){
			
				$this->phoneNumber;
				$this->adress;
				$this->request;
		}
		
		function placeRequest(){
		
		}

		function setAdress($address){
		
		}
		
		function setPhoneNumber($phoneNumber){
		
		}
		
		function isFavorite($propId){ 
			$favoriteProperties = get_user_meta($uid,'aspk_favorite_homes',true);
			if(!empty($favoriteProperties)){
				if(in_array($propId, $favoriteProperties)){
					return true;
				}
			}
			return false;
		}
		
		function setFavorite($propId,$uid){
		
		$favoriteProperties = get_user_meta($uid,'aspk_favorite_homes',true);
			if (empty($favoriteProperties)) $favoriteProperties = array();
			$favoriteProperties[] = $propId;
			update_user_meta( $uid,'aspk_favorite_homes', $favoriteProperties);
			
			return $favoriteProperties;
			
		}
		
		function unSaveProperty($propId,$uid){
			$favoriteProperties= array();
			$fProperties=get_user_meta($uid,'aspk_saved_homes',true);
			foreach($fProperties as $fp){
				if($fp==$propId) continue;
				$favoriteProperties[]=$fProperties;	
			}
			update_user_meta( $uid,'aspk_saved_homes', $removeProperty);
			return $removeProperty;
		
		}
		
		function getIsHidden($propId,$uid){
			$hiddenProperties = get_user_meta($uid,'aspk_hidden_homes',true);
			if(!empty($hiddenProperties)){
				if(in_array($propId, $hiddenProperties)){
					return true;
				}
			}
			return false;
		}
		
		function hideProperty($propId,$uid){
			$hiddenProperties = get_user_meta($uid,'aspk_hidden_homes',true);
			if (empty($hiddenProperties)) $hiddenProperties = array();
			$hiddenProperties[] = $propId;
			update_user_meta( $uid,'aspk_hidden_homes', $hiddenProperties);
			return $hiddenProperties;
		}
		
		function unHideProperty($propId,$uid){
			$hiddenProperties= array();
			$hproperties=get_user_meta($uid,'aspk_hidden_homes',true);
			foreach($hproperties as $hp){
				if($hp==$propId) continue;
				$hiddenProperties[]=$hp;	
			}
			update_user_meta( $uid,'aspk_hidden_homes', $hiddenProperties);
			return $hiddenProperties;
		}
		
		function saveProperty($propId,$uid){
			$properties = get_user_meta($uid,'aspk_saved_homes',true);
			if (empty($properties)) $properties = array();
			$properties[] = $propId;
			update_user_meta( $uid,'aspk_saved_homes', $properties);
			
			return $properties;
		}
		
		function shareProperty($share_name,$share_email,$y_name,$y_email,$prop_id,$mls_id){	
			add_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));
			
			$email_template = $this->sendShareEmail($share_name,$y_name,$prop_id,$mls_id);
			$headers = 'From: agilesolutionspk.com/qa17' . "\r\n";
			wp_mail( $share_email,'Your friend suggested this listing.', $email_template,$headers);
			remove_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));
		}
		
		function sendShareEmail($share_name,$y_name,$prop_id,$mls_id){
			$page = get_page_by_title('View Result');
			$siteUrl = get_permalink($page->ID);
			$sharelink = $siteUrl.'/?matrixUniqueId='.$prop_id.'&mlsId='.$mls_id;
			ob_start();
			?>
			<div style="clear:left;width:100%;padding:1em;">
				<div><h2>Dear <?php echo $share_name; ?></h2></div>
				<div><p><?php echo $y_name.' '; ?>has suggested that the following real estate listing might be of interest to you! </p></div>
				<div><p>If you want to view this property. Please <a href="<?php echo $sharelink; ?>">Click here </a></p></div>
			</div>
			<?php
			 $email_template = ob_get_clean();
			 return $email_template;
		}
		
		function set_html_content_type() {
				return 'text/html';
		}
		
		function saveSearch($uid,$searchName,$sql){
			if($uid){
				$save_search = array();
				$save_search = get_user_meta($uid,'_aspk_save_results_search',true);
				if (array_key_exists($searchName,$save_search)){
					$ret = array('st' => 'error','msg'=>'Search not saved, Search name already exist try with other name','sql'=>$sql);
					echo json_encode($ret);
					exit;
				}
				if(empty($save_search)){
					$save_search[$searchName] = $sql;
					update_user_meta($uid,'_aspk_save_results_search',$save_search);
					$ret = array('st' => 'ok','msg'=>'Search Saved','sql'=>$sql);
					echo json_encode($ret);
					exit;
				}else{
					$save_search[$searchName] = $save_search;
					$save_search[$searchName] = $sql;
					update_user_meta($uid,'_aspk_save_results_search',$save_search);
					$ret = array('st' => 'ok','msg'=>'Search Saved','sql'=>$sql);
					echo json_encode($ret);
					exit;
				}
			}else{
				$ret = array('st' => 'fail','msg'=>'Search not Saved');
				echo json_encode($ret);
				exit;
			}
		}
		
		function unSaveSearch($uid,$searchName){
			$search = get_user_meta($uid,'_aspk_save_results_search',true);
			$newSearch = array();
			foreach($search as $sk=>$sv){
				
				if($sk==$searchName)continue;{
					$newSearch[$sk]=$sv;
				}
			update_user_meta( $uid,'_aspk_save_results_search', $newSearch);
			}
			return $newSearch;
		}
		
		function getSearchList($uid){
			$search = get_user_meta($uid,'_aspk_save_results_search',true);
			return $search;
		}
		
		function getSearch($searchName,$uid){
			$search = get_user_meta($uid,'_aspk_save_results_search',true);
			foreach($search as $sk=>$sv){
				if($sk==$searchName){
					return $sv;
				}
				return false;
			}
		}
		
		function updateSearchLastRun($searchName,$datetime,$numResults,$uid){
			
			if(is_int($numResults)){
				$search = get_user_meta($uid,'aspk_saved_search',true);
				//check for not empty
				if(!empty($search)){
					$searchDetail = $search[$searchName];
					//if empty return false
					if(empty($searchDetail)){
						return false;
					}
					$searchDetail['lastRun'] = $datetime;//use passed one
					$searchDetail['numResults'] = $numResults;
					
					$search[$searchName] = $searchDetail;
					update_user_meta( $uid,'aspk_saved_search', $search);
					return true;
				}
						
			}
			return false;
				
		}
		
		function setSearchLastRun($searchName,$datetime,$numResults,$uid){
			$search = get_user_meta($uid,'_aspk_save_results_search',true);
			foreach($search as $sk=>$sv){
				if(($sk==$searchName)&&($numResults>=1)) {
					return false;
				}else{
				$searchDetail['name']=$searchName;
				$searchDetail['searchKv']=$search[$sk]['searchKv'];
				$searchDetail['lastRun'] = date('d-m-y h:i:s');
				$searchDetail['createdDate'] = date('d-m-y h:i:s');
				$updateSearch[$searchName]=$searchDetail;
				update_user_meta( $uid,'_aspk_save_results_search', $updateSearch);
				return true;
				}
			}
				
		}		
		
		function shareSearch($searchId){
			add_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));
			$page = get_page_by_title('View Result');
			$siteUrl = get_permalink($page->ID);
			$sharelink = '<a href='.$siteUrl. '/?search_id='.$searchId.'> click here to view property</a>';
			wp_mail( 'hassaniqbal.aspk@gmail.com', 'link check', $sharelink );
			remove_filter( 'wp_mail_content_type', array(&$this,'set_html_content_type' ));
			return $sharelink;
		
		}
		
	}
}
?>
