<?php
if(!class_exists('AspkDb')){
	
	class AspkDb implements IAspkDb{
		protected $pkey;
		protected $searchFeilds ;
		protected $detaillFeilds;
		protected $allowedFields = array();
		
		function AspkDb(){
			$this->searchFeilds = array() ;
			$this->detailFeilds = array() ;
				
		}
		function getPrimaryKey(){
		
		return $pkey;
		}
		function setAllowedFields(){
			
		}
		
		function install(){
		//
		}
	
		function  getResultFields(){
			
		
		return $feilds;
		}
		
		function setResultFields($feilds){
			
		}
		
		function  getDetailResultFields(){
		
		
		return $dtlFeilds;
		}
		
		function setDetailResultFields($feilds){
			
		}
		
		function getFeilds(){
			return $this->allowedFields;	
		}
		
	    function validateField($field){
			if(in_array($field, $this->allowedFields)){
			return true;
			}
		}
		function validateValue($fieldName,$value){
		
			$value = strtolower($value);
		
			switch($fieldName){
				case 'price':
				if(is_numeric($value)){
					return true;
				}
				continue;
				case 'mlsNumber':
					if(is_numeric($value)){
						return true;
					}
				continue;
				case 'bedRooms':
					if(is_numeric($value)){
						return true;
					}
				continue;
				case 'bath':
					if(is_numeric($value)){
						return true;
					}
				continue;
				case 'postalCode':
					if(is_numeric($value)){
						return true;
					}
				continue;
				case 'approximateSqftSize':
					if(is_numeric($value)){
						return true;
					}
				continue;
				default:
					return true;
				break;
			}
			return false;
		}
	
		function cleanPost($feilds){
			//$fileds is kv array
			$postSql = new Aspk_Post_To_Sql();
			$cleanPost = array();
			foreach($feilds as $feild=>$value){
				$cfeild = $postSql->getField($feild);
				$csuffix = $postSql->getSuffix($feild);
				if(! $this->validateField($cfeild)) continue;
				if($csuffix){
				$cleanPost[$cfeild.'-'.$csuffix] = $value;
				}else{
					$cleanPost[$cfeild]=$value;
				}
			}
			return $cleanPost;
		}
		
		function rowsAffected(){
			global $wpdb;
			$rowsEffected=$wpdb->rows_affected;
			return $rowsEffected;
			
		}
	}
}
	?>