<?php
 if(!class_exists('FmlsDb')){
	
	class FmlsDb extends AspkDb{
		
		protected $pkey;
		protected $searchFeilds = array() ;
		protected $detailFeilds =array();
		protected $allowedFields = array();
		Private $error = array();
		protected $setError;
		private $rows_affected;
		
	function FmlsDb(){
		$this->pkey = 'matrixUniqueId';
		register_activation_hook( 'RT/aspk_Rt_controller.php', array(&$this, 'install') );
		$this->setAllowedFields();
		$this->setResultFields();
		$this->setDetailResultFields();
		$this->allowedFields;
		}
		
	function install(){
		global $wpdb;
			$sql = "CREATE TABLE IF NOT EXISTS  `".$wpdb->prefix."aspkRtPropertyDb` (
			`id` INT( 50 ) NOT NULL ,
			`matrixUniqueId` INT( 50 ) NOT NULL ,
			`mlsNumber` INT( 50 ) NOT NULL ,
			`propertyType` VARCHAR( 255 ) NOT NULL ,
			`propertySubType` VARCHAR( 255 ) NULL ,
			`bedRooms` VARCHAR( 100 ) NOT NULL ,
			`baths` VARCHAR( 100 ) NOT NULL ,
			`area` TEXT NOT NULL ,
			`address` TEXT NOT NULL ,
			`postalCode` INT( 20 ) NOT NULL ,
			`description` LONGTEXT NULL ,
			`currentPrice` VARCHAR( 50 ) NOT NULL ,
			`style` VARCHAR( 255 ) NULL ,
			`approximateLotSize` VARCHAR( 100 ) NOT NULL ,
			`approximateSqftSize` VARCHAR( 100 ) NOT NULL ,
			`parkingDesc` LONGTEXT NULL ,
			`taxYear` DATE NULL ,
			`Neighbourhood` VARCHAR( 255 ) NULL ,
			`schools` VARCHAR( 255 ) NULL ,
			`yearBuilt` DATE NULL ,
			`status` VARCHAR( 100 ) NOT NULL ,
			`propertyImg` VARCHAR( 255 ) NOT NULL ,
			PRIMARY KEY (  `id` )
			) ENGINE = INNODB COMMENT =  'RTpropertydb' AUTO_INCREMENT=1
			";
			$sql = $wpdb->query($sql);

			$sql="CREATE TABLE `".$wpdb->prefix."aspkLog` (
			`id` INT( 11 ) NOT NULL AUTO_INCREMENT PRIMARY KEY ,
			`date` DATE NOT NULL ,
			`ip_adress` VARCHAR( 100 ) NOT NULL ,
			`city` VARCHAR( 150 ) NULL ,
			`beds` VARCHAR( 100 ) NULL ,
			`baths` VARCHAR( 100 ) NULL ,
			`price_from` VARCHAR( 100 ) NULL ,
			`price_to` VARCHAR( 100 ) NULL ,
			`form_feilds` LONGTEXT NOT NULL
			) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ";
			$sql = $wpdb->query($sql);
		}
	
		function setAllowedFields(){
			$this->allowedFields = array("matrixUniqueId","mlsNumber","propertyType","propertySubType","bedRooms","baths",
				"area","address","postalCode","description","currentPrice","style","approximateLotSize",
				"approximateSqftSize","parkingDesc","taxYear","neighbourhood","schools","yearBuilt","status","propertyImg");
		}
		
		function setResultFields($fields = false){
			
			if(is_array($fields)){
				
				$this->searchFeilds = array();
				
				foreach($fields as $f){
					foreach($f as $k=> $v){
						if($this->validateField($k)){
							$this->searchFeilds[$k] = $v;
						}
					}
				}
			}else{
				$this->searchFeilds = array('propertyType','propertySubType','bedRooms','baths','area','address','postalCode','currentPrice','Neighbourhood','status','propertyImg');
			}
		}
		
		function setDetailResultFields($fields = false){
			if(is_array($fields)){
				
				$this->detailFeilds = array();
				
				foreach($fields as $f){
					if($this->validateField($f)){
						$this->detailFeilds[] = $f;
					}
				}
				
			}else{
			$this->detailFeilds = array('matrixUniqueId','mlsNumber','propertyType','propertySubType','bedRooms','baths',
				'area','address','postalCode','description','currentPrice','style','approximateLotSize',
				'approximateSqftSize','parkingDesc','taxYear','Neighbourhood','schools','yearBuilt','status','propertyImg');
			
			}
		}
		
		
		function getResultFields(){
			return $this->searchFeilds;
		}
		
		function getDetailResultFields(){
			return $this->detailFeilds;
		}
		function getPrimaryKey(){
			 $pkey = $this->pkey;
			return $pkey;
		}
		function getProperty($propertyId){
			global $wpdb; 
			$sql="SELECT * FROM  `{$wpdb->prefix}aspkRtPropertyDb` WHERE {$this->pkey} ={$propertyId}";
			//check for false or empty and return false if needed
			$ret = $wpdb->get_row($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			return $wpdb->get_row($sql);
		}
		
		function getPropertyKv($field,$value){
			global $wpdb; 
			$sql="SELECT * FROM  `{$wpdb->prefix}aspkRtPropertyDb` WHERE {$field} = '{$value}'";
			$ret = $wpdb->get_row($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			return $wpdb->get_row($sql);
		}
		
		function insertProperty($alldata){
			global $wpdb; 
			$fields="";
			$values="";
			$fieldx="";
			$valuex="";
			foreach($alldata as $k=>$v){
				if(!$v) continue;
			 	$fieldx .= $k.',';
				$valuex .= "'{$v}',";	
			}
			$fields = substr($fieldx, 0, -1);
			$values = substr($valuex, 0, -1);
			$sql="INSERT INTO {$wpdb->prefix}aspkRtPropertyDb ({$fields}) VALUES({$values})";
			$ret = $wpdb->query($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			global $wpdb; 
			$rows=$this->rowsAffected();
			return true;
		
		}
		function updateProperty($propertyId,$alldata){
			$arrKv = array();
			foreach($alldata as $key=>$val){
				if(!$val) continue;
				$arrKv[] = "{$key} = '{$val}'";
			}
			$str = implode(' , ', $arrKv);
			global $wpdb; 	
			$sql="UPDATE {$wpdb->prefix}aspkRtPropertyDb set {$str} WHERE {$this->pkey}='{$propertyId}'";
			$ret = $wpdb->query($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			return true;
		}
		
		function deleteProperty($propertyId){
			global $wpdb; 
			$sql="DELETE FROM {$wpdb->prefix}aspkRtPropertyDb WHERE {$this->pkey} ={$propertyId}";
			$ret = $wpdb->query($sql);
			if(!$ret){
				$this->setError = $wpdb->last_error;
				return false;
			}
			return true;
		}
		
		
		function getTabelName(){
			global $wpdb; 
			$tabelName = $wpdb->prefix.'aspkRtPropertyDb';
			return $tabelName;
		}
	
	}

 }
 
 ///$wpdb->rows_affected
?>