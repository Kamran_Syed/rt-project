<?php
interface IAspkDb{
	public function getPrimaryKey();
	public function install();
	public function setAllowedFields();
	function getResultFields();
	function setResultFields($feilds);
	function getDetailResultFields();
	function setDetailResultFields($feilds);
	function validateField($field);
	function cleanPost($feilds);
}

?>