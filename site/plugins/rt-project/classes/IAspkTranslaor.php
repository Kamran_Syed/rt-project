<?php

interface IAspkTranslaor{
	public function getSql($kv);
	public function setLimit($limit);
	public function setPage($page = 1);
	function getPageSql($kv);
	function getDetailSql($kv);
}

?>
