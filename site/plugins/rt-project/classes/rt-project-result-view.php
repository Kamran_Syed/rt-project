<?php
if ( !class_exists( 'Aspk_Results_View22' )){

	class Aspk_Results_View22{
		
		function __construct(){
			
		}
		
		function show_user_profile(){
			$user_id = get_current_user_id();
			$user_info = get_user_meta( $user_id, '_aspk_user_profile',true);
			$save_search = get_user_meta( $user_id, '_aspk_save_results_search',true);
			if (! is_user_logged_in() ) {
				return "Please login <a href='".home_url()."/wp-login.php'>here</a>";
			}else{
				$current_user = wp_get_current_user();
				$nickname = get_user_meta( $user_id, 'nickname',true);
					?>
				<div class="tw-bs container">
					<div id="tabs" style = "border:none !important;">
						<div id="wait" style = "display:none;">
						   <img src="<?php echo plugins_url('images/loader.gif', __FILE__); ?>" width="150" height="150" style="margin-top:250px;opacity:1"/>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12">
								<ul>
									<li><a style="color:#000000 !important;" href="#tabs-1">Profile</a></li>
									<li><a style="color:#000000 !important;" href="#tabs-2">Saved Properties</a></li>
									<li><a style="color:#000000 !important;" href="#tabs-3">Hidden Properties</a></li>
									<li><a style="color:#000000 !important;" href="#tabs-4">Saved Search List</a></li>
								<!--	<li><a style="color:#000000 !important;" href="#tabs-5">Search Notification List</a></li>-->
								</ul>
							</div>
						</div>
						<div id="tabs-1">
							<div class="row aspk_row">
								<div class="col-md-12">
									<form action="" method="post">
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Name</div>
											<div class="col-md-9 aspk_col"><input id = "aspk_name" type = "hidden" name = "wp_user_id" value = "<?php echo $user_id; ?>" />
												<input type = "text" name = "user_name" value = "<?php echo $nickname; ?>" />
											</div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Email</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "email" value = "<?php echo $current_user->user_email; ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Phone Number</div>
											<div class="col-md-9 aspk_col"><input id = "aspk_phone" type = "text" name = "phone" value = "<?php if($user_info['phone-number']) { echo $user_info['phone-number']; }  ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Home Street</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "h_st" value = "<?php if($user_info['house-street']) { echo $user_info['house-street'];} ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Home City</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "h_cty" value = "<?php  if($user_info['house-city']) { echo $user_info['house-city'];} ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Home State</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "h_stat" value = "<?php  if($user_info['house-state']) { echo $user_info['house-state'];} ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Home Zip</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "h_zip" value = "<?php if($user_info['house-zipcode']) { echo $user_info['house-zipcode'];} ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Work Street</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "w_st" value = "<?php  if($user_info['work-street']) { echo $user_info['work-street'];} ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Work City</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "w_cty" value = "<?php if($user_info['work-city']) { echo $user_info['work-city'];} ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Work State</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "w_stat" value = "<?php if($user_info['work-state']) { echo $user_info['work-state'];}  ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-3 aspk_col">Work Zip</div>
											<div class="col-md-9 aspk_col"><input type = "text" name = "w_zip" value = "<?php if($user_info['work-zipcode']) { echo $user_info['work-zipcode'];}  ?>" /></div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-12 aspk_col"><input type = "submit" name = "update_profile" value = "Update" /></div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<div id="tabs-2">
							<div class="row aspk_row">
								<div class="col-md-12">
									<?php
									$user_saved_homes = get_user_meta( $user_id, 'aspk_saved_homes',true); 
									if($user_saved_homes){
										foreach($user_saved_homes as $home_id){
											$home_results = $this->getProperty_by_matrixId($home_id);
											?>
											<div id="agile_hide_div_<?php echo $home_id; ?>">
												<div class="row aspk_row">
													<div class="col-md-12 aspk_col"><img class="aspk_img_width" src="<?php echo $home_results->propertyImg; ?>" ></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12 aspk_col"><?php echo $home_results->address; ?></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12"><b>Price: &nbsp;</b><?php echo $home_results->currentPrice; ?></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12"><b>Beds: &nbsp;</b><?php echo $home_results->bedRooms; ?>&nbsp;<b>Baths: &nbsp;</b><?php echo $home_results->baths; ?></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12"><b>Description</b></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12"><?php echo $home_results->description; ?></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12"><?php echo $home_results->description; ?></div>
												</div>
												<div class="row aspk_row">
													<div class="col-md-12 aspk_col"><input type="button" value="Unsave" onclick="aspk_unsave_home_<?php echo $home_id;?>()"></div>
												</div>
											</div>
											<script>
												function aspk_unsave_home_<?php echo $home_id;?>(){
													jQuery("#wait").css("display","block");
													var data = {
														'action': 'aspk_unsave_prop',
														'property_id': <?php echo $home_id; ?>         
													};
													
													var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
													jQuery.post(ajaxurl, data, function(response) {
														jQuery("#wait").css("display","none");
														obj = JSON.parse(response);
														 if(obj.st == 'ok'){		
															jQuery('#agile_hide_div_<?php echo $home_id; ?>').hide();
														 }
														  if(obj.msg){	
															alert(obj.msg);	
														 }
													});
												}
											</script>
											<?php
										}  	
									}  	
									?>
								</div>
							</div>
						</div>
						<div id="tabs-3">
							<div class="row aspk_row">
								<div class="col-md-12">
									<div class="row aspk_row">
										<div class="col-md-12">
											<?php
											$hidden_homes = get_user_meta( $user_id, 'aspk_hidden_homes',true);
											if($hidden_homes){
												foreach($hidden_homes as $hidden_home){
													$hidden_results = $this->getProperty_by_matrixId($hidden_home);
													?>
												<div id="agile_prop_hide_div_<?php echo $hidden_home; ?>">
													<div class="row aspk_row">
														<div class="col-md-12 aspk_col"><img class="aspk_img_width" src="<?php echo $hidden_results->propertyImg; ?>" ></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12 aspk_col"><?php echo $hidden_results->address; ?></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12"><b>Price: &nbsp;</b><?php echo $hidden_results->currentPrice; ?></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12"><b>Beds: &nbsp;</b><?php echo $hidden_results->bedRooms; ?>&nbsp;<b>Baths: &nbsp;</b><?php echo $hidden_results->baths; ?></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12"><b>Description</b></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12"><?php echo $hidden_results->description; ?></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12"><?php echo $hidden_results->description; ?></div>
													</div>
													<div class="row aspk_row">
														<div class="col-md-12 aspk_col"><input type="button" value="Unhide" onclick="aspk_unhide_home_<?php echo $hidden_home; ?>()"></div>
													</div>
												</div>
												<script>
													function aspk_unhide_home_<?php echo $hidden_home; ?>(){
														jQuery("#wait").css("display","block");
														var data = {
															'action': 'aspk_remove_hidden_prop',
															'property_id': <?php echo $hidden_home; ?>         
														};
														
														var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
														jQuery.post(ajaxurl, data, function(response) {
															jQuery("#wait").css("display","none");
															obj = JSON.parse(response);
															 if(obj.st == 'ok'){		
																jQuery('#agile_prop_hide_div_<?php echo $hidden_home; ?>').hide();
															 }
															  if(obj.msg){	
																alert(obj.msg);	
															 }
														});
													}
												</script>
													<?php
												}  	  
											}  	  
											?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div id="tabs-4">
							<div class="row aspk_row">
								<div class="col-md-12">
									<?php
									$save_sqls = get_user_meta($user_id,'_aspk_save_results_search',true);
									if($save_sqls){
										foreach($save_sqls as $k=>$val){
											$key = str_replace(' ','_',$k);
										?>
										<div id="view_delete_btn_<?php echo $key; ?>">
											<div class="row aspk_row">
												<div class="col-md-3"><div class="aspk_float_left"><?php echo $k; ?></div></div>
												<div class="col-md-9">
													<div class="aspk_float_left"><input type="button" value="View"  onclick="agile_show_results_<?php echo $key; ?>()">&nbsp;&nbsp;&nbsp;</div>
													<div class="aspk_float_left"><input type="button" value="Delete" onclick="agile_delete_results_<?php echo $key; ?>()"></div>
												</div>
											</div>
										</div>
										<div class="row aspk_row">
											<div class="col-md-12">
												<div id="aspk_121_<?php echo $key; ?>" style="display:none;">
													<?php 
													$pageview = get_page_by_title('View Result');
													$link_url = get_permalink($pageview->ID);
													$all_results = $this->get_all_saved_search_res($val); 
													if($all_results){
														foreach($all_results as $property){
														?>
														<div class="row aspk_row">
															<div class="col-md-4 aspk_col">
																<img src="<?php echo $property->propertyImg; ?>" class="aspk_p_img">
															</div>
															<div class="col-md-8 aspk_col">
																<div class="row aspk_row">
																	<div class="col-md-12"><a href="<?php echo $link_url.'?matrixUniqueId='. $property->matrixUniqueId .'&mlsId='.$property->mlsNumber; ?>"><?php  echo $property->address; ?></a></div>
																</div>
																<div class="row aspk_row">
																	<div class="col-md-12"><p class="aspk_p_tag"><?php  echo $property->description; ?></p></div>
																</div>
																<div class="row aspk_row">
																	<div class="col-md-12"><?php  echo $property->currentPrice; ?></div>
																</div>
																<div class="row aspk_row">
																	<div class="col-md-12"><?php  echo $property->area; ?></div>
																</div>
															</div>
														</div>
														<?php
														}
													}
													
													?>
												</div>
											</div>
										</div>
										<script>
											function agile_show_results_<?php echo $key; ?>(){
												jQuery("#aspk_121_<?php echo $key; ?>").toggle();
											}
											function agile_delete_results_<?php echo $key; ?>(){
												jQuery("#wait").css("display","block");
												var data = {
													'action': 'aspk_delete_saved_prop',
													'property_key': '<?php echo $k; ?>'         
												};
												
												var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
												jQuery.post(ajaxurl, data, function(response) {
													jQuery("#wait").css("display","none");
													obj = JSON.parse(response);
													 if(obj.st == 'ok'){		
														jQuery('#view_delete_btn_<?php echo $key; ?>').hide();
														jQuery('#aspk_121_<?php echo $key; ?>').hide();
													 }
													  if(obj.msg){	
														alert(obj.msg);	
													 }
												});
											}
										</script>
										<?php
										}
									}
									?>
								</div>
							</div>
						</div>
					<!--<div id="tabs-5">
							<div class="row aspk_row">
								<div class="col-md-12">5th tab
								</div>
							</div>
						</div>-->
					</div>
				</div><!-- end container -->
					<script>
						jQuery( document ).ready(function() {
							jQuery(function() {
								jQuery( "#tabs" ).tabs();
							});
						});
					</script>
					<?php
			}
		}
		
		function get_all_saved_search_res($sql){
			global $wpdb; 
			
			return $wpdb->get_results($sql);
		}
		
		function getProperty_by_matrixId($matrixId){
			global $wpdb; 
			
			$sql="SELECT * FROM  `{$wpdb->prefix}aspkRtPropertyDb` WHERE matrixUniqueId = '{$matrixId}'";
			return $wpdb->get_row($sql);
			
		}
		
		private function show_save_and_hide_property($propertyid,$mls_id){
			ob_start();
			if ( is_user_logged_in() ){
			?>
			<div class="tw-bs container">
				<div class="row aspk_row">
					<div class="col-md-6 aspk_col">&nbsp;</div>
					<div class="col-md-6 aspk_col">
						<div class="aspk_float_left">
							<input type="button" onclick="aspk_show_property_<?php echo $propertyid; ?>()" value="Save Property" name="aspk_submit_save_p" class="btn_submit_color">
						</div>
						<div class="aspk_float_left">
							<input type="button" onclick="aspk_hide_property_<?php echo $propertyid; ?>()" value="Hide Property" name="aspk_submit_hide_p" class="btn_submit_color">
						</div>
						<div class="aspk_float_left">
							<input type="button" onclick="aspk_share_my_property_<?php echo $propertyid; ?>()" value="Share Property" name="aspk_submit_share_p" class="btn_submit_color">
						</div>
					</div>
				</div>
				<div id="show_dialoge" style="display:none;"></div>
				<div class="row aspk_row" id="aspk_show_form_<?php echo $propertyid; ?>" title="Share this Property to your friend" style="display:none" >
					<div style="clear:left;" id="aspk_share_info_<?php echo $propertyid;?>"><!-- start share from -->
						<div class="row aspk_row"  style="" >
							<div class="col-md-6 aspk_col">Your Friend's Name</div>
							<div class="col-md-6 aspk_col">
								<input type="text" name="aspk_f_name_<?php echo $propertyid; ?>" id = "friend_name_<?php echo $propertyid; ?>" />
							</div>
						</div>
						<div class="row aspk_row" style="" >
							<div class="col-md-6 aspk_col">Your Friend's Email</div>
							<div class="col-md-6 aspk_col">
								<input type="text"  name="aspk_f_email_<?php echo $propertyid; ?>" id = "friend_emial_<?php echo $propertyid; ?>" />
							</div>
						</div>
						<div class="row aspk_row"  style="" >
							<div class="col-md-6 aspk_col">Your Name</div>
							<div class="col-md-6 aspk_col">
								<input type="text" name="aspk_name_<?php echo $propertyid; ?>" id = "your_name_<?php echo $propertyid; ?>"/>
							</div>
						</div>
						<div class="row aspk_row" style="" >
							<div class="col-md-6 aspk_col">Your Email</div>
							<div class="col-md-6 aspk_col">
								<input type="text" name="aspk_email_<?php echo $propertyid; ?>" id="your_emial_<?php echo $propertyid; ?>" />
							</div>
						</div>
						<div class="row aspk_row" style="" >
							<div class="col-md-12 aspk_col">
								<input type = "button" class="btn btn-primary" id = "submit_share" value = "Send Email" onclick="share_property_email_<?php echo $propertyid; ?>()"/>
							</div>
						</div>
					</div><!-- end share from -->
					<div style="clear:left;display:none;" id="aspk_share_reply_<?php echo $propertyid;?>"><!-- start share reply -->
						<div class="row aspk_row"  >
							<div class="col-md-12 aspk_col">
								<div id="aspk_reply_<?php echo $propertyid; ?>"></div>
							</div>
						</div>
					</div><!-- end share reply -->
				</div>
				<script>
				
					
					function aspk_show_property_<?php echo $propertyid; ?>(){
						jQuery("#wait").css("display","block");
						var data = {
							'action': 'aspk_save_detail',
							'prop_id': <?php echo $propertyid; ?>         
						};
						
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							jQuery("#wait").css("display","none");
							obj = JSON.parse(response);
							 if(obj.msg){	
								jQuery( "#show_dialoge" ).html(obj.msg);
								jQuery( "#show_dialoge" ).dialog();
							 }
						});
					}
					
					
					function share_property_email_<?php echo $propertyid; ?>(){
						jQuery("#wait").css("display","block");
						var fname = jQuery('[name="aspk_f_name_<?php echo $propertyid; ?>"]').val();
						var f_email = jQuery('[name="aspk_f_email_<?php echo $propertyid; ?>"]').val();
						var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
						var y_name = jQuery('[name="aspk_name_<?php echo $propertyid; ?>"]').val();
						var y_email = jQuery('[name="aspk_email_<?php echo $propertyid; ?>"]').val();
						if(fname.length < 1) {
							jQuery("#wait").css("display","none");
							alert('Please enter your friend name'); 
							return;
						}
						
						if (testEmail.test(f_email)){
							
						}else{ 
							jQuery("#wait").css("display","none");
							alert('Please enter valid email address');
							return;
						}
						
						if(y_name.length < 1) {
							jQuery("#wait").css("display","none");
							alert('Please enter your name'); 
							return;
						}
						
						if (testEmail.test(y_email)){
							
						}else{
							jQuery("#wait").css("display","none");
							alert('Please enter valid email address');
							return;
						}
						var data22 = {
							'action': 'aspk_share_property',
							'property_id' : <?php echo $propertyid; ?>,
							'mls_id' : <?php echo $mls_id; ?>,
							'friend_name' : fname,
							'friend_emial' : f_email,
							'your_name' : y_name,
							'your_emial' : y_email
						};
							
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data22, function(response) {
							jQuery("#wait").css("display","none");
							obj = JSON.parse(response);
							 if(obj.msg='share Success'){	
								jQuery('#aspk_share_info_<?php echo $propertyid; ?>').hide();
								jQuery('#aspk_share_reply_<?php echo $propertyid; ?>').show();
								jQuery('#aspk_reply_<?php echo $propertyid; ?>').html('<b>Dear ' + obj.your_name + '</b><p>Thank you for your referral.Your message has been sent to ' + obj.friend_name + '</p><p>We hope that your friend will find this listing helpful.</p><p>Your business is important to us.</p><p>Sincerely,</p><p>Agile Solutions Pakistan.</p>');
							 }
						});
					}
					function aspk_hide_property_<?php echo $propertyid; ?>(){
						jQuery("#wait").css("display","block");
						var data = {
							'action': 'aspk_hide_prop_detail',
							'property_id': <?php echo $propertyid; ?>      
						};
						var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
						jQuery.post(ajaxurl, data, function(response) {
							jQuery("#wait").css("display","none");
							obj = JSON.parse(response);
							if(obj.msg){	
								jQuery( "#show_dialoge" ).html(obj.msg);
								jQuery( "#show_dialoge" ).dialog();
							 }
						});
						
					}
					function aspk_share_my_property_<?php echo $propertyid; ?>(){
						jQuery( "#aspk_show_form_<?php echo $propertyid; ?>" ).dialog({width: 425,height:420});
					}
				</script>
			</div>
			<?php 
			}
			$html = ob_get_clean();
			return $html;
		}
		
		private function show_property($property){
			ob_start();
			?>
			<div class="tw-bs container">
				<div class="row aspk_row">
					<div class="col-md-5 aspk_col">
						<img src="<?php echo $property->propertyImg; ?>" class="aspk_p_img">
					</div>
					<div class="col-md-7 aspk_col">
						<div class="row aspk_row">
							<div class="col-md-12"><a href="<?php echo get_permalink().'?matrixUniqueId='. $property->matrixUniqueId .'&mlsId='.$property->mlsNumber; ?>"><?php  echo $property->address; ?></a></div>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12"><p class="aspk_p_tag"><?php  echo $property->description; ?></p></div>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12"><?php  echo $property->currentPrice; ?></div>
						</div>
						<div class="row aspk_row">
							<div class="col-md-12"><?php  echo $property->area; ?></div>
						</div>
					</div>
				</div>
			</div >
			<?php 
			$html = ob_get_clean();
			return $html;
		}
		
		function show_results($all_results,$sql){
			if($all_results){
				foreach($all_results as $property){
					echo $this->show_property($property);
					echo $this->show_save_and_hide_property($property->matrixUniqueId,$property->mlsNumber);
				}
			?>	
			<div id="wait" style = "display:none;">
			   <img src="<?php echo plugins_url('images/loader.gif', __FILE__); ?>" width="150" height="150" style="margin-top:250px;opacity:1"/>
		    </div>
			<div style="display:none;clear:left;" id="save_msg"></div>
			<div style="display:none;clear:left;" id="error_msg">Please write search name and then continue.</div>
				<div class="row aspk_row">
					<div class="col-md-4 aspk_col">&nbsp;</div>
					<div class="col-md-8 aspk_col">
						<div class="aspk_float_left">Search Name </div>
						<div class="aspk_float_left"><input id="agile_search_name" class="aspk_search_field"  type="text" name="search_name" value=""></div>
						<div class="aspk_float_left">
							<input type="hidden" value="<?php echo $sql; ?>" id="aspk_save_sql">
							<input type="hidden" value="<?php echo get_current_user_id(); ?>" id="aspk_user_id">
							<input type="button" name="save_search" class="btn_search_class" value="Save Search" onclick="save_search_123()">			
						</div>
					</div>
				</div >
				<script>
					var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' );?>';
					function save_search_123(){
						var x = jQuery('#agile_search_name').val();
						if(x == false){
							jQuery("#error_msg").dialog();
						}else{
							jQuery("#wait").css("display","block");
							var data = {
								'action'   : 'aspk_save_search',
								'search_name' : jQuery('#agile_search_name').val(),
								'user_id' : jQuery('#aspk_user_id').val(),
								'search_sql' : jQuery("#aspk_save_sql").val()
							};
							
							jQuery.post(ajaxurl, data, function(response) {
								obj = JSON.parse(response);
								jQuery("#wait").css("display","none");
								if(obj.st == 'ok'){
									jQuery('#save_msg').html('Search has been saved');
									//alert(obj.sids);
									jQuery('#search_id').val(obj.sql);
									jQuery('#save_msg').dialog();
								}else if(obj.st == 'error'){
									jQuery('#save_msg').html(obj.msg);
									jQuery('#save_msg').dialog();
								}else{
									jQuery('#save_msg').html('Search not Saved');
									jQuery('#save_msg').dialog();
									jQuery('#search_id').val(obj.sql);
								}
								
							});
						}
					}
				</script>
			<?php
			}
		}
		
		function show_detail_page($detail){
			
			?>
			<div class="tw-bs container">
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><img src="<?php echo $detail->propertyImg; ?>" class="aspk_p_img"></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span"><?php echo $detail->address; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span"><?php echo $detail->area; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Status:</span><span class="aspk_span"><?php echo $detail->status; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Price:</span><span class="aspk_span"><?php echo $detail->currentPrice; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Property Type:</span><span class="aspk_span"><?php echo $detail->propertyType; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><span class="aspk_span">Neighbourhood:</span><span class="aspk_span"><?php echo $detail->Neighbourhood; ?></span></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><h1 class="aspk_h1_tag">Description:</h1></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><p class="aspk_p_tag"><?php echo $detail->description; ?></p></div>
				</div>
				<div class="row aspk_row">
					<div class="col-md-12 aspk_col"><h1 class="aspk_h1_tag">Directions:</h1></div>
				</div>
				 <script src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
								<script>
								  var geocoder;
								  var map;
								  var mapOptions = {
									  zoom: 14,
									  zoomControl: true,
									  mapTypeId: google.maps.MapTypeId.ROADMAP
									}
								  var marker;

								  jQuery(document).ready(function ($) {
										geocoder = new google.maps.Geocoder();
										map = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
										codeAddress();
								  });

								  function codeAddress() {
									var address = "<?php echo $detail->address ?>";
									geocoder.geocode( { 'address': address}, function(results, status) {
									  if (status == google.maps.GeocoderStatus.OK) {
										map.setCenter(results[0].geometry.location);
										if(marker)
										  marker.setMap(null);
										marker = new google.maps.Marker({
											map: map,
											position: results[0].geometry.location,
											draggable: true
										});
									  } else {
										alert('Geocode was not successful for the following reason: ' + status);
									  }
									});
								  }
								</script>
					<div class="row aspk_row">
						<div class="col-md-12 aspk_col" id="map_canvas"></div>
					</div>
					<div class="row aspk_row">
						<div class="col-md-12 aspk_col"><p class="aspk_p_tag"><?php echo $detail->address;?></p></div>
					</div>
			</div>
			<?php
		}
		function show_search_form22(){
				$page = get_page_by_title('View Result');
				$p_link = get_permalink($page->ID);
			?>

			<div class="tw-bs container">

				<form   method="post" action="<?php echo $p_link; ?>">

					<div id ="basic_form">

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Property</span>

								<span class="aspk_span">

									<select name="propertyType-Any" >

										<option value="ATT,DET" >House & Condo/Townhouse</option>

										<option value="DET" >House Only</option>

										<option value="ATT" >Condo/Townhouse Only</option>

									</select>

								</span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">City</span>

								<span class="aspk_span"><input  type="text" name="area"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Zip Code</span>

								<span class="aspk_span"><input  type="text" name="postalCode"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Min Price</span>

								<span class="aspk_span"><input  type="text" name="currentPrice-From"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Max Price</span>

								<span class="aspk_span"><input type="text" name="currentPrice-UpTo"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Beds</span>

								<span class="aspk_span">

									<select name="bedRooms-From" >

										<option value="" >Any</option>

										<option value="1" >1+</option>

										<option value="2" >2+</option>

										<option value="3" >3+</option>

										<option value="4" >4+</option>

										<option value="5" >5+</option>

									</select>

								</span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Baths</span>

								<span class="aspk_span">

									<select name="baths-From" >

										<option value="" >Any</option>

										<option value="1" >1+</option>

										<option value="2" >2+</option>

										<option value="3" >3+</option>

										<option value="4" >4+</option>

										<option value="5" >5+</option>

									</select>

								</span>

							</div>

						</div>

					</div>

					<!--adv form-->

					<?php

						$stl = 'style="display:none;"';

					?>

							<div id="aspk_advance_search" <?php echo $stl;?>>

								<div class="row aspk_row">

									<div class="col-md-12"><span>Neighborhood/Complex:</span>

										<span class="aspk_span"><input  type="text" name="Neighbourhood-Like"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="bedroomDescription" value="MASTR" /></span><span>Master on Main</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="BasementDesc-Any" value="BATH,BTDOR,BTHST,CRAWL,DAYLT,DRVWY,EXTEN,FINSH,FULL,INTER,PARTL,UNFIN" /></span><span>Basement</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="Stories" value="ONEST" /></span><span>One Story</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="SpecialCircumstances-All" value="RTCOM" /></span><span>Adult Community</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="WaterfrontFootage" value="1+" /></span><span>Waterfront</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="PoolonProperty-Any" value="ABOVE,FIBER,GUNIT,HEATD,INGRN,VINYL" /></span><span>Pool</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="TennisonPropertyYN" value="1" /></span><span>Tennis Court</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span">Square Footage Min:</span>

										<span class="aspk_span"><input type="text" name="approximateSqftSize-From"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span">Square Footage Max:</span>

										<span class="aspk_span"><input type="text" name="approximateSqftSize-UpTo"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12"><span class="aspk_span">Parking</span>

										<span class="aspk_span">

											<select name="parkingDesc-Any">

												<option value="">Any</option>

												<option value="1GARG,2GARG,4GARG,GARGE" >Garage</option>

												<option value="CPORT,1CARP,2CARP" >Carport</option>

												<option value="DRVWY,PRKPD" >Driveway</option>

												<option value="STRET" >Street</option>

											</select>

										</span>

									</div>

								</div>	

								<div class="row aspk_row">

									<div class="col-md-12" style="clear:left;display:none">

										<span class="aspk_span">Monthly HOA Fees Max:</span>

										<span class="aspk_span"><input  type="number" name="MonthlyAssocFee-UpTo"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12" style="clear:left;display:none"><span class="aspk_span">Days on Market:</span>

										<span class="aspk_span" >

											<select name="aspk_days_on_market" >

												<option value="" >Any</option>

												<option value="new_listings">New Listings</option>

												<option value="less_than_3_days">Less than 3 days</option>

												<option value="less_than_7_days">Less than 7 days</option>

												<option value="less_than_15_days">Less than 15 days</option>

												<option value="less_than_30_days">Less than 30 days</option>

												<option value="more_than_60_days,">More than 60 days</option>

												<option value="more_than_90_days,">More than 90 days</option>

												<option value="more_than_180_days,">More than 180 days</option>

											</select>

										</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12" ><span class="aspk_span">Price Reduced:</span>

										<span class="aspk_span">

											<select name="PriceChangeTimestamp-From">

												<option value="">Any</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-259200);?>">In last 3 days</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-604800);?>">In last 7 days</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-1296000);?>">In last 15 days</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-2592000);?>">In last 30 days</option>

											</select>

										</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12" ><span class="aspk_span">Exclude Short Sales:</span>

										<span class="aspk_span"><input  id="exclude_ss" type="checkbox" name="SpecialCircumstances-Not" value="SSPAP,POTSS" /></span>

									</div>

								</div>

							</div>

					<!--adv ends-->

					

					<div><input type="hidden" name="Status-Any" value="A" /></div>

						<div class="row aspk_row">

							<div class="col-md-12" ><input type="submit" class="btn btn-primary" name="aspk_search" value="Search" /></div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12" ><a href="<?php echo get_permalink();?>">Reset Search</a></div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12 aspk_col" >

								<input type="button" class="btn btn-primary basadv" id="aspk_advance_btn" value="Advance Search" onclick="show_advance_fields();" >

								<input type="button" onclick="basic_search();" id="aspk_basic_btn" class="btn btn-primary" style="display:none;"  value="Basic Search" >

							</div>	

						</div>	

				</form>

				<script>

					function show_advance_fields(){

						jQuery("#aspk_advance_search").show();

						jQuery("#aspk_basic_btn").show();

						jQuery("#aspk_advance_btn").hide();

					}

					

					function basic_search(){

						jQuery("#aspk_advance_search").hide();

						jQuery("#aspk_basic_btn").hide();

						jQuery("#basic_form").show();

						jQuery("#aspk_advance_btn").show();

					}

				</script>

			</div>

			<?php		
			
		}
		function show_search_form11(){
				$page = get_page_by_title('View Result');
				$p_link = get_permalink($page->ID);
			?>

			<div class="tw-bs container">

				<form   method="post" action="<?php echo $p_link; ?>">

					<div id ="basic_form">

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Property</span>

								<span class="aspk_span">

									<select name="propertyType-Any" >

										<option value="ATT,DET" >House & Condo/Townhouse</option>

										<option value="DET" >House Only</option>

										<option value="ATT" >Condo/Townhouse Only</option>

									</select>

								</span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">City</span>

								<span class="aspk_span"><input  type="text" name="area"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Zip Code</span>

								<span class="aspk_span"><input  type="text" name="postalCode"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Min Price</span>

								<span class="aspk_span"><input  type="text" name="currentPrice-From"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Max Price</span>

								<span class="aspk_span"><input type="text" name="currentPrice-UpTo"  /></span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Beds</span>

								<span class="aspk_span">

									<select name="bedRooms-From" >

										<option value="" >Any</option>

										<option value="1" >1+</option>

										<option value="2" >2+</option>

										<option value="3" >3+</option>

										<option value="4" >4+</option>

										<option value="5" >5+</option>

									</select>

								</span>

							</div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12"><span class="aspk_span">Baths</span>

								<span class="aspk_span">

									<select name="baths-From" >

										<option value="" >Any</option>

										<option value="1" >1+</option>

										<option value="2" >2+</option>

										<option value="3" >3+</option>

										<option value="4" >4+</option>

										<option value="5" >5+</option>

									</select>

								</span>

							</div>

						</div>

					</div>

					<!--adv form-->

					<?php

						$stl = 'style="display:none;"';

					?>

							<div id="aspk_advance_search" <?php echo $stl;?>>

								<div class="row aspk_row">

									<div class="col-md-12"><span>Neighborhood/Complex:</span>

										<span class="aspk_span"><input  type="text" name="Neighbourhood-Like"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="bedroomDescription" value="MASTR" /></span><span>Master on Main</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="BasementDesc-Any" value="BATH,BTDOR,BTHST,CRAWL,DAYLT,DRVWY,EXTEN,FINSH,FULL,INTER,PARTL,UNFIN" /></span><span>Basement</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="Stories" value="ONEST" /></span><span>One Story</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="SpecialCircumstances-All" value="RTCOM" /></span><span>Adult Community</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="WaterfrontFootage" value="1+" /></span><span>Waterfront</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="PoolonProperty-Any" value="ABOVE,FIBER,GUNIT,HEATD,INGRN,VINYL" /></span><span>Pool</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span"><input  type="checkbox" name="TennisonPropertyYN" value="1" /></span><span>Tennis Court</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span">Square Footage Min:</span>

										<span class="aspk_span"><input type="text" name="approximateSqftSize-From"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12">

										<span class="aspk_span">Square Footage Max:</span>

										<span class="aspk_span"><input type="text" name="approximateSqftSize-UpTo"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12"><span class="aspk_span">Parking</span>

										<span class="aspk_span">

											<select name="parkingDesc-Any">

												<option value="">Any</option>

												<option value="1GARG,2GARG,4GARG,GARGE" >Garage</option>

												<option value="CPORT,1CARP,2CARP" >Carport</option>

												<option value="DRVWY,PRKPD" >Driveway</option>

												<option value="STRET" >Street</option>

											</select>

										</span>

									</div>

								</div>	

								<div class="row aspk_row">

									<div class="col-md-12" style="clear:left;display:none">

										<span class="aspk_span">Monthly HOA Fees Max:</span>

										<span class="aspk_span"><input  type="number" name="MonthlyAssocFee-UpTo"  /></span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12" style="clear:left;display:none"><span class="aspk_span">Days on Market:</span>

										<span class="aspk_span" >

											<select name="aspk_days_on_market" >

												<option value="" >Any</option>

												<option value="new_listings">New Listings</option>

												<option value="less_than_3_days">Less than 3 days</option>

												<option value="less_than_7_days">Less than 7 days</option>

												<option value="less_than_15_days">Less than 15 days</option>

												<option value="less_than_30_days">Less than 30 days</option>

												<option value="more_than_60_days,">More than 60 days</option>

												<option value="more_than_90_days,">More than 90 days</option>

												<option value="more_than_180_days,">More than 180 days</option>

											</select>

										</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12" ><span class="aspk_span">Price Reduced:</span>

										<span class="aspk_span">

											<select name="PriceChangeTimestamp-From">

												<option value="">Any</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-259200);?>">In last 3 days</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-604800);?>">In last 7 days</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-1296000);?>">In last 15 days</option>

												<option value="<?php echo date('Y-m-d\TH:i:s', time()-2592000);?>">In last 30 days</option>

											</select>

										</span>

									</div>

								</div>

								<div class="row aspk_row">

									<div class="col-md-12" ><span class="aspk_span">Exclude Short Sales:</span>

										<span class="aspk_span"><input  id="exclude_ss" type="checkbox" name="SpecialCircumstances-Not" value="SSPAP,POTSS" /></span>

									</div>

								</div>

							</div>

					<!--adv ends-->

					

					<div><input type="hidden" name="Status-Any" value="A" /></div>

						<div class="row aspk_row">

							<div class="col-md-12" ><input type="submit" class="btn btn-primary" name="aspk_search" value="Search" /></div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12" ><a href="<?php echo get_permalink();?>">Reset Search</a></div>

						</div>

						<div class="row aspk_row">

							<div class="col-md-12 aspk_col" >

								<input type="button" class="btn btn-primary basadv" id="aspk_advance_btn" value="Advance Search" onclick="show_advance_fields();" >

								<input type="button" onclick="basic_search();" id="aspk_basic_btn" class="btn btn-primary" style="display:none;"  value="Basic Search" >

							</div>	

						</div>	

				</form>

				<script>

					function show_advance_fields(){

						jQuery("#aspk_advance_search").show();

						jQuery("#aspk_basic_btn").show();

						jQuery("#aspk_advance_btn").hide();

					}

					

					function basic_search(){

						jQuery("#aspk_advance_search").hide();

						jQuery("#aspk_basic_btn").hide();

						jQuery("#basic_form").show();

						jQuery("#aspk_advance_btn").show();

					}

				</script>

			</div>

			<?php		
			
		}
		
	} //class ends
}//if class ends