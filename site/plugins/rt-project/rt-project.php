<?php
/*
Plugin Name: RT Project
Plugin URI: 
Description: This is specially used for Real Estate
Author: Agile Solutions PK
Version: 1.0
Author URI: http://agilesolutionspk.com
*/
require_once(__DIR__ ."/classes/IAspkDb.php"); 
require_once(__DIR__ ."/classes/IAspkPerson.php"); 
require_once(__DIR__ ."/classes/IAspkResults.php"); 
require_once(__DIR__ ."/classes/IAspkTranslaor.php"); 
require_once(__DIR__ ."/classes/aspk_post_to_sql.php");
require_once(__DIR__ ."/classes/aspk_rt_post_view.php"); 
require_once(__DIR__ ."/classes/AspkDb.php");
require_once(__DIR__ ."/classes/AspkFilter.php");  
require_once(__DIR__ ."/classes/AspkFmlsDb.php");  
require_once(__DIR__ ."/classes/AspkPerson.php"); 
require_once(__DIR__ ."/classes/AspkProperty.php"); 
require_once(__DIR__ ."/classes/AspkResults.php"); 
require_once(__DIR__ ."/classes/AspkTranslaor.php"); 
require_once(__DIR__ ."/classes/AspkUser.php"); 
require_once(__DIR__ ."/classes/AspkLog.php"); 
require_once(__DIR__ ."/classes/rt-project-result-view.php");

if ( !class_exists( 'Agile_Rt_Real_Estate' )){

	class Agile_Rt_Real_Estate{
		
		function __construct(){
			register_activation_hook( __FILE__, array($this, 'install') );
			register_deactivation_hook( __FILE__, array($this, 'de_activate') );
			add_shortcode('aspk_rt_view', array(&$this,'View_Results'));
			add_shortcode( 'aspk_rt_search', array(&$this,'search_rt'));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts'));
			add_shortcode( 'aspk_rt_search_box', array(&$this,'webix_search_box'));
			add_action( 'admin_menu', array(&$this,'admin_menu' ) );
			add_action('wp_ajax_aspk_save_detail', array(&$this,'rt_save_property12')); // for logged in user
			add_action( 'wp_ajax_nopriv_aspk_save_detail',  array(&$this,'rt_save_property12' ));
			add_action('wp_ajax_aspk_hide_prop_detail', array(&$this,'rt_hide_property')); // for logged in user
			add_action( 'wp_ajax_nopriv_aspk_hide_prop_detail',  array(&$this,'rt_hide_property' )); 
			add_action('wp_ajax_aspk_unsave_prop', array(&$this,'unsaved_property')); // for logged in user
			add_action( 'wp_ajax_nopriv_aspk_unsave_prop',  array(&$this,'unsaved_property' ));
			add_action('wp_ajax_aspk_remove_hidden_prop', array(&$this,'remove_hidden_property')); // for logged in user
			add_action( 'wp_ajax_nopriv_aspk_remove_hidden_prop',  array(&$this,'remove_hidden_property' ));
			add_action('wp_ajax_aspk_delete_saved_prop', array(&$this,'delete_saved_search')); // for logged in user
			add_action( 'wp_ajax_nopriv_aspk_delete_saved_prop',  array(&$this,'delete_saved_search' ));
			add_action( 'wp_ajax_aspk_share_property', array(&$this,'sharePropertyCall' ));
			add_action( 'wp_ajax_nopriv_aspk_share_property',  array(&$this,'sharePropertyCall' ));
			add_action( 'wp_ajax_aspk_save_search', array(&$this,'save_search' ));
			add_action( 'wp_ajax_nopriv_aspk_save_search',  array(&$this,'save_search' ));
			
			add_shortcode( 'aspk_user_dashboard', array(&$this,'user_dashboard'));
		}
		
		function user_dashboard(){
			if(isset($_POST['update_profile'])){
				$wp_user_id = $_POST['wp_user_id'];
				$name = $_POST['user_name'];
				$email = $_POST['email'];
				$phone = $_POST['phone'];
				$h_street = $_POST['h_st'];
				$h_city = $_POST['h_cty'];
				$h_state = $_POST['h_stat'];
				$h_zip = $_POST['h_zip'];
				$w_street = $_POST['w_st'];
				$w_city = $_POST['w_cty'];
				$w_state = $_POST['w_stat'];
				$w_zip = $_POST['w_zip'];
				if(!empty($email)){
					$args = array(
						'ID'         => $wp_user_id,
						'user_email' => esc_attr( $_POST['email'] )
					);
					wp_update_user( $args );
				}
				$user_information = array();
				$user_information['phone-number'] = $phone;
				$user_information['house-street'] = $h_street;
				$user_information['house-city'] = $h_city;
				$user_information['house-state'] = $h_state;
				$user_information['house-zipcode'] = $h_zip;
				$user_information['work-street'] = $w_street;
				$user_information['work-city'] = $w_city;
				$user_information['work-state'] = $w_state;
				$user_information['work-zipcode'] = $w_zip;
				update_user_meta( $wp_user_id, '_aspk_user_profile',$user_information);
			}
			$rs_view = new Aspk_Results_View22();
			$rs_view->show_user_profile();
			
		}
		function share_property(){
			
			
			
		}
		function save_search(){
			$searchName = $_POST['search_name'];
			$uid = $_POST['user_id'];
			$sql = $_POST['search_sql'];
			$aspk_user = new AspkUser();
			$aspk_user->saveSearch($uid,$searchName,$sql);
		}
		
		function admin_menu(){
			add_menu_page( 'Scraper Data', 'Scraper Data', 'manage_options', 'scraper_data', array(&$this,'scraper_data'));
			
		}
		
		function rt_save_property12(){
			$fmlsDb = new FmlsDb();
			$aspk_user = new AspkUser();
			$propId = $_POST['prop_id'];
			$uid=get_current_user_id();
			$sProperties=get_user_meta($uid,'aspk_saved_homes',true);
			if($sProperties){
					if(in_array($propId,$sProperties)){
						$ret = array('act' => 'save_prop','st'=>'error','msg'=>'Property already saved');
						echo json_encode($ret);
					}else{
						$uid= get_current_user_id();
						$result = $aspk_user->saveProperty($propId,$uid);
						$ret = array('act' => 'save_prop','st'=>'ok','msg'=>'Property has been saved');
						echo json_encode($ret);
					}
					exit;
				
			}else{
				$uid= get_current_user_id();
				$result = $aspk_user->saveProperty($propId,$uid);
				$ret = array('act' => 'save_prop','st'=>'ok','msg'=>'Property has been saved');
				echo json_encode($ret);
			}
			exit;
		}
		
		function delete_saved_search(){
			$fmlsDb = new FmlsDb();
			$aspk_user = new AspkUser();
			$propkey = $_POST['property_key'];
			$uid=get_current_user_id();
			$aspk_user->unSaveSearch($uid,$propkey); 
			
			$ret = array('act' => 'hide_prop','st'=>'ok','msg'=>'Property has been Unsaved', 'aspk_hide'=>'hide_div');
			echo json_encode($ret);
			exit;	
			
		}
		
		function unsaved_property(){
			$fmlsDb = new FmlsDb();
			$aspk_user = new AspkUser();
			$propId = $_POST['property_id'];
			$uid=get_current_user_id();
			$aspk_user->unSaveProperty($propId,$uid); 
			$ret = array('act' => 'hide_prop','st'=>'ok','msg'=>'Property has been Unsaved', 'aspk_hide'=>'hide_div');
			echo json_encode($ret);
			exit;	
			
		}
		
		function remove_hidden_property(){
			$fmlsDb = new FmlsDb();
			$aspk_user = new AspkUser();
			$propId = $_POST['property_id'];
			$uid=get_current_user_id();
			$aspk_user->unHideProperty($propId,$uid);
			$ret = array('act' => 'hide_prop','st'=>'ok','msg'=>'Property has been removed from hidden properties', 'aspk_hide'=>'hide_div');
			echo json_encode($ret);
			exit;	
		}
		
		function rt_hide_property(){
			$fmlsDb = new FmlsDb();
			$aspk_user = new AspkUser();
			$propId = $_POST['property_id'];
			$uid=get_current_user_id();
			$sProperties=get_user_meta($uid,'aspk_hidden_homes',true);
			if($sProperties){
					if(in_array($propId,$sProperties)){
						$ret = array('act' => 'hide_prop','st'=>'error','msg'=>'Property already hidden');
						echo json_encode($ret);
					}else{
						$uid= get_current_user_id();
						$result = $aspk_user->hideProperty($propId,$uid);
						$ret = array('act' => 'hide_prop','st'=>'ok','msg'=>'Property has been hidden');
						echo json_encode($ret);
					}
					exit;
				
			}else{
				$uid= get_current_user_id();
				$result = $aspk_user->hideProperty($propId,$uid);
				$ret = array('act' => 'hide_prop','st'=>'ok','msg'=>'Property has been hidden');
				echo json_encode($ret);
			}
			exit;
		}
		
		function sharePropertyCall(){
			$aspk_user = new AspkUser();
			
			$prop_id = $_POST['property_id'];
			$mls_id = $_POST['mls_id'];
			$f_name = $_POST['friend_name'];
			$f_email = $_POST['friend_emial'];
			$y_name = $_POST['your_name'];
			$y_email = $_POST['your_emial'];
			
			$aspk_user->shareProperty($f_name,$f_email,$y_name,$y_email,$prop_id,$mls_id);
			
			$ret = array('act' => 'share_property','st'=>'ok','msg'=>'share Success', 'your_name'=>$y_name , 'friend_name'=> $f_name);
			echo json_encode($ret);
			exit;
		}
		
		function dummyDatastub(){
			$dummyData = array();
			//$dummyData[];
			$dummyData['matrixUniqueId'] = '80';
			$dummyData['mlsNumber'] = '45';
			$dummyData['propertyType'] = 'ATT,DTT';
			$dummyData['propertySubType'] ='aprtment';
			$dummyData['bedRooms'] = '2';
			$dummyData['baths'] ='3';
			$dummyData['area'] ='dummy';
			$dummyData['address'] ='flat kasjdflk  df fgf';
			$dummyData['postalCode'] ='10001';
			$dummyData['description'] ='this ai s itjdlskj dflkj';
			$dummyData['currentPrice'] ='460';
			$dummyData['style'] = 'standard';
			$dummyData['approximateLotSize'] = '20' ;
			$dummyData['approximateSqftSize'] = '60';
			$dummyData['parkingDesc'] = '1GARG' ;
			$dummyData['taxYear'] = '12-12-1995';
			$dummyData['neighbourhood'] ='lords';
			$dummyData['schools'] = 'jfjshschool';
			$dummyData['yearBuilt'] = '21-12-1955';
			$dummyData['status'] = 'active';
			$dummyData['propertyImg'] = 'gfdgdgdg';
			
			return $dummyData;
		}
		
		
		function scraper_data(){
			require_once("scraper.php");
		}
		
		function webix_search_box(){
			$v = new Aspk_Results_View22();
			
			?>
			<div id="splash">
				<div class="row">
					<div class="col-md-12"><h2 class="aspk_align_center">WELCOME HOME</h2></div>
				</div>
				<div class="row" id="aspk_container">
					<div class="col-md-12"></div>
				</div>
			</div>
	
			<script type="text/javascript">
			
				webix.ui({
					type:"line",
					container:"aspk_container",
					rows: [
							
						  { view:"toolbar", id:"mybar", elements:[
							{ view:"select", id:"aspk_single" ,name:"billa_g",  width:150, options:[
								{ id:1, value:"Rent" },
								{ id:2, value:"Buy" },            
								{ id:3, value:"Commercial" },            
							]  }, 
							{ view:"select", id:"aspk_location",  width:150, options:[
								{ id:1, value:"London" },
								{ id:2, value:"Uk" },            
								{ id:3, value:"Bermingum" },            
							]  }, 
							{ view:"multiselect", id:"multi_neighborhood",  placeholder:"Neighborhood22", width:200, options:[
								{ id:1, value:"Bay Ridge" },
								{ id:2, value:"BedFord" },            
								{ id:3, value:"Boerum Hill" },            
							]},
							{ view:"select", id:"aspk_bed",  width:150, options:[
								{ id:1, value:"One" },
								{ id:2, value:"Two" },            
								{ id:3, value:"Three" },            
								{ id:4, value:"Four" },            
								{ id:5, value:"Four +" },            
							]  }, 
							{ view:"select", id:"aspk_min_price", placeholder:"Min-Price", width:150, options:[
								{ id:1, value:"1000" },
								{ id:2, value:"2000" },            
								{ id:3, value:"3000" },            
								{ id:4, value:"4000" },            
							]},
							{ view:"select", id:"aspk_max_price", placeholder:"Max-Price", width:150, options:[
								{ id:1, value:"1000" },
								{ id:2, value:"2000" },            
								{ id:3, value:"3000" },            
								{ id:4, value:"4000" },            
							]},
							{ view:"button", value:"Search", width:70 },
							]
						}
					]
				});

			</script>
			<?php 
		}
		
		function View_Results(){
			$rs = $this->summary_result();
			return $rs;
		}
		
		function search_rt(){
			$this->basic_search();
			
		}
		
		function install(){
			$aspkFmlsDb = new FmlsDb();
			$aspkFmlsDb->install();
		}
		
		function de_activate(){
			
		}
		
		function basic_search(){
			$aspk_rs_view = new Aspk_Results_View22();
			//$aspk_rs_view->show_search_form22();
			$aspk_rs_view->show_search_form11();
			
		}
		
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('jquery-ui');
			wp_enqueue_script('jquery-ui-dialog');
			wp_enqueue_style( 'tw-bs', plugins_url('css/tw-bs.3.1.1.css', __FILE__) );
			wp_enqueue_style( 'jquery-ui-css', plugins_url('css/jquery-ui.css', __FILE__) );
			wp_enqueue_style( 'aspk_rt_project_css', plugins_url('css/rt-project.css', __FILE__) );
			wp_enqueue_style( 'aspk_rt_webix_css', plugins_url('css/webix.css', __FILE__) );
			wp_enqueue_script( 'agile_boot_files', plugins_url('js/bootstrap.js', __FILE__) );
			wp_enqueue_script( 'agile_jssor_slider', plugins_url('js/jssor.js', __FILE__) );
			wp_enqueue_script( 'agile_jssor_slider_files', plugins_url('js/jssor.slider.js', __FILE__) );
			wp_enqueue_script( 'agile_jssor.slider.min', plugins_url('js/jssor.slider.min.js', __FILE__) );
			wp_enqueue_script( 'agile_webix_js', plugins_url('js/webix.js', __FILE__) );
			wp_enqueue_script( 'agile_jquery_ui_js', plugins_url('js/agile-jquery-ui.js', __FILE__) );
		}
		
		function summary_result(){
			global $wpdb;
			
			$aspk_rs_view = new Aspk_Results_View22();
			$fmlsDb = new FmlsDb();
			$aspkUser = new AspkUser();
			$postToSql = new Aspk_Post_To_Sql();
			$aspkLog = new AspkLog();
			$propertyId = '42112';
			$aspkProperty = new AspkProperty($propertyId);
			if(isset($_POST['aspk_search'])){
				$postFields=$_POST;
				$cleanFeilds=$fmlsDb->cleanPost($postFields);
				$logFeilds=$aspkLog->getLogFeilds();
				$aspkLog->saveLogFeilds($cleanFeilds);
				$sql=$postToSql->getSql($cleanFeilds); 
				$results=$wpdb->get_results($sql);
				$uid=get_current_user_id();
				$searchDetail = array('name'=>'my search');
				$search =$aspkUser->getSearchList($uid);
				$searchName='my hgjfirst search';
				//$srch=$aspkUser->getSearch($searchName,$uid);
				$datetime='09-07-15 06:19:39';
				$numResults = '1';
				//$aspkUser->setSearchLastRun($searchName,$datetime,$numResults,$uid);
				if(!$results){
				echo 'No result found.';
				}else{
				
				//$searchlink=$aspkUser->shareSearch($lastInsertedId);
				$rs = $aspk_rs_view->show_results($results,$sql);
				return $rs;
				}
				//$fmlsDb->setResultFields($results);
				//$ResultFields=$fmlsDb->getResultFields();
				return;
			}
			if(isset($_GET['mlsId'])){
				$mlsid = $_GET['mlsId'];
				$matrixUniqueId = $_GET['matrixUniqueId'];
				$detail = $fmlsDb->getProperty($matrixUniqueId);
				$rs = $aspk_rs_view->show_detail_page($detail);
				return $rs;
			}
		}
		
	} //class ends
}//if class ends

$agile_bt =new Agile_Rt_Real_Estate();