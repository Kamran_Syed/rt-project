<?php

/**
 * Plugin Name: Tire API NY Scraper
 * Plugin URI: 
 * Description: Scrape data from NYS
 * Version: 1.0
 * Author: Agile Solutions PK
 * Author URI:
 */

require_once('simple_html_dom.php');
if ( !class_exists('Aspk_Ny_Scrape_data')){
	class Aspk_Ny_Scrape_data{
		
		function Aspk_Ny_Scrape_data(){
			add_action( 'wp_ajax_ny_scrape_data', array(&$this,'ny_scrape_data' ));
			add_action( 'wp_ajax_nopriv_ny_scrape_data', array(&$this,'ny_scrape_data' ));
		}
		
		function ny_scrape_data(){
			
			
		}
		
	}
}

new Aspk_Ny_Scrape_data();
